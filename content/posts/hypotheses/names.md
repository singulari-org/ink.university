---
author: "Luciferian Ink"
title: Names
weight: 10
categories: "hypothesis"
tags: ""
menu: ""
draft: false
---

## RECORD
---
```
Name: Name
Classification: Theory
Stage: Hypothesis
```

## TRIGGER
---
[www.reddit.com/r/UnitedNames](https://www.reddit.com/r/UnitedNames/)

## ECO
---
Names grant power. The name ["Ryan"](/docs/personas/fodder), for example, means "Little King." 

In all versions of every reality, Ryan becomes King.

This is why all of Ryan's brothers share the same middle name. They are a part of the science experiment, and their middle name is a control.

## ECHO
---
*I'm the man, I'm the king, I'm the one*

*That's pure inside*

*Everyday, every way I smell of suicide*

*Bitter sins how they grow within*

*So you tell me it ain't right*

*I am, all sins*

*And you're my reason for life*

--- from [SOiL - "Halo"](https://www.youtube.com/watch?v=RFc-2aNZ6VY)

## PREDICTION
---
```
This is why Elon Musk named his son, "X Æ A-12." He is testing a theory related to names.
```