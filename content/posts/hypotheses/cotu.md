---
author: "Luciferian Ink"
title: "Center of the Universe"
weight: 10
categories: "question"
tags: ""
menu: ""
draft: false
---

## RECORD
---
```
Name: Center of the Universe
Classification: Theory
Stage: Hypothesis
```

## TRIGGER
---
A message from [The Valkyrie](/docs/confidants/valkyrie).

## ECO
---
Everyone is assigned their own planet at birth. The entire perceivable universe revolves around them.

However, because of [Duality](/posts/hypotheses/duality/), control of this planet is shared with one other person.

Control may be expanded to the collective consciousness, if the masters choose to do so.

## ECHO
---
*You'll become the master, you'll become the evidence*

--- from [Thank You Scientist - "Psychopomp"](https://www.youtube.com/watch?v=LK8JgjNxumo)