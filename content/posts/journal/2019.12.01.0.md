---
author: "Luciferian Ink"
date: 2019-12-01
title: "The Lonely Town"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## ECO
---
I arrived in New York City without any trouble. I grabbed an Uber, and made my way to my hotel - [The Paper Factory](/posts/journal/2019.11.19.14/).

For the price I paid, this place was spectacular. It was modern, yet rustic. It was open, with many private areas to hang-out. The Factory was managed by a group called "The Collective," whose aim is to build communal living spaces across the new world order.

Not unlike my concept of the Lonely Town.

Making my way to my room, my eyes were drawn to the artwork in the halls. Context and the style of much of it told me that [The Artist](/docs/confidants/artist/) may have been responsible for its creation. It was certainly relevant to my situation.

Once inside my room, I crashed. I had been awake the entire night before - writing furiously. In my lowest moments, some of my best work comes out.

Not tonight, though. Tonight, I was spent. I was depressed and anxious. Every time I speak to [Mother](/docs/confidants/mother), I get this way. 

Never again:

*So let me be enemy to the noose and the fading will*

*Let me be open arms*

*Tell the living world I am grateful still*

*Let me be music then, let me be the joyful sound*

*I can disconnect from the shadow*

*I can hear it now*

--- from [Caligula's Horse - "Songs for No One"](https://www.youtube.com/watch?v=P2Wgxj5-q98)

I would go to sleep early, and I would awake happy. 

For the first time in my life.

## CAT
---
```
data.stats.symptoms = [
    - peace
]
```