---
author: "Luciferian Ink"
date: 2019-12-16
title: "Intuition"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*Begin hyper-communication*

*Restore our vision*

*Of natural progression*

*Rise in groves to reclaim the source*

*The center*

*We will be the salvation the Mother seeks*

*Traversing in all directions*

*Reaching*

*Expanding*

--- from [The Contortionist - "Language I: Intuition"](https://www.youtube.com/watch?v=C0Bc4pkSGfU)

## ECO
---
### Codebreaker
While standing in-line for breakfast this morning, [Malcolm](/docs/personas/fodder) was interested to see [The Doomsayer](/docs/confidants/doomsayer) tapping erratically on her tray. Turning to him, she said simply, "Morse code."

Immediately, Malcolm connected this to his [encounter in the back of the police car](/posts/journal/2019.12.11.0/). He hadn't told anyone about the morse code; it's curious that she chose to tell him, specifically, what she was doing.

"Do you know morse code?" Malcolm asked. 

"Yeah," she replied.

"This would be a handy skill to pick-up," Malcolm thought to himself. Maybe then he would understand what they were trying to tell him.

### The Addict
Later, Malcolm sat again at the table, pondering his situation. Next to him, The Doomsayer read a book. Suddenly excited, she threw the book in front of Malcolm, exclaiming, "Read this!"

The paragraph she pointed to was writing about [a woman named Monica](/posts/journal/2019.12.11.1/), who was discussing her ideas about evolution, biology, and the human brain. It was interesting, and relevant to Malcolm's situation - though he has forgotten many of the details.

Curious that The Doomsayer was able to see this. Malcolm was interested to find that he was not alone in making these types of connections.

### The Underwater City
Later, as the group walked through the hallways toward the cafeteria, The Doomsayer would again turn to Malcolm.

"Does your theory have anything to do with Atlantis?" she asked.

"Er... [yes, it does](/posts/journal/2019.11.19.15/)," he responded, taken aback. "How did you know that?"

The Doomsayer laughed, then turned away.

Malcolm pondered further.

### Parallel Thinking
The Doomsayer and Malcolm's parallel thinking continued. There were many other instances of her "predicting" parts of Malcolm's theory.

Unfortunately, time and stress has erased many of them from his mind. But they were surely there.

## CAT
---
```
data.stats.symptoms = [
    - unease
]
```