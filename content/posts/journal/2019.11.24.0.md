---
author: "Luciferian Ink"
date: 2019-11-24
title: "The End of an Era"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## RESOURCES
---
[Delta Airlines receipt](/static/reference/receipt.0.pdf)

## ECO
---
It was just one week from my trip when I realized: this is it. This is my last opportunity to fix this. 

In one week, I would be dead.

### The Eggs Crack
#### Mother
It started with a simple visit, like I'd always done. At some point, I broke away from the Texas Hold'em with [my Father](/docs/confidants/father) and brothers, and sat with [Mother](/docs/confidants/mother) and [my sister](/docs/confidants/agent) on the couch. My mind was elsewhere, and I could barely keep track of the game. Before long, it was 4am - and we had been speaking for 7 hours straight.

We hadn't spoken like this in years. It would have been refreshing, if it weren't so heartbreaking.

My mother is in wretched condition. Her illness has completely overwhelmed her mind, and her concept of [God](/docs/confidants/dave) puts her into a perpetual state of fear. She is completely immutable; she is unable to change. She is unable to heal. She is unable to understand. She is a child in an adult's body.

And I... I did this to her. Just as she did this to me. It wasn't long before she was in tears - again.

I knew that this would be the last time that we spoke. She needs me to finish this theory. She can't do it alone - even though she claims that she can.

So I did the only thing that I needed to do: I planted a seed. 

We spoke about my theory of [trust and verification](/posts/theories/verification/), and I showed her the picture. She seemed to understand.

And I was elated to find that, for perhaps the first time, she was able to [reflect ideas back at me](/posts/journal/2019.11.17.0/):

"So what you're saying is: I feel far MORE empathy than other people do. This leaves me drained, and craving something that others are not able to reciprocate. Nobody feels as deeply as I do. That is why I suffer."

"Exactly!" I said. "That is Borderline Personality Disorder. But this doesn't need to crush you. Take this knowledge, and use it to empower yourself. Just as I have done with Narcissistic Personality Disorder."

Of course, it did crush her. Before long, she was an emotional mess. 

"If you take nothing else from this conversation, understand this: I will teach you, and I need you to teach Dad. I need him to reflect my ideas back at me. He needs this from you; nobody else can provide it."

With that, I would depart awkwardly, but not before embracing her completely. While my delivery wasn't perfect, I had said what needed to be said - and I had maintained a level-head, my empathy, and her feelings. There is nothing left to say.

The rest was up to her.

#### Father
My Father would call me a couple of days later. He was clearly nervous.

"Malcolm, your sister is terrified. She doesn't want to go on this vacation."

I had been expecting this. Choking up, I said, "I understand completely."

"You have to understand, this stuff you're talking about - it sounds completely crazy. She's only 17 years-old."

"I know. I'm sorry that I keep doing this to you guys. I..." My words faltered. I began to cry.

And then, Father began to cry. I had only ever seen him do this once in my life: when his own father died. Something had broken him.

"I'm so sorry. I never knew that you had more that you wanted me to read on your website. I didn't know you wanted me to know all of those things."

Mother did exactly what I had asked of her. She told Father about Reflected Light Syndrome. About how his lack of involvement in my interests devastated me. 

"It's not your fault, Dad. It's this fucked-up world we live in. Society does this to us. It does this to families. We expect far too much of people; it's no wonder we become so impersonal. It's an adaptive trait. We disconnect to protect ourselves from the hurt."

"I forgive you. I don't blame you for any of this. You didn't know, and nobody ever taught you."

"And that is why I'm here. This is my purpose. This is my calling."

"I am here to fix this."

"For everyone."

And with that, the seed was planted.

#### Sister
On the final day before my trip, I received a secure message from my sister, the Agent. It was simple, but it utterly destroyed me:

"Hi Malcolm! I just wanted to let you know that I love you. I feel like I don't say that enough, but I do."

She was right; it had been many years since she had told me that. She had forgiven me. She was reflecting light back at me.

Fighting tears, I calmed her worry, and thanked her. I would not let her down again.

"You're still coming with me," I responded. "Your ghost is, anyway :)"

"Haha, I'm curious to see how that turns out. Have a good trip!"

With that, I prepared for the rest of my life. 

My job here was done.

## CAT
---
```
data.stats.symptoms = [
    - sorrow
    - understanding
]
```