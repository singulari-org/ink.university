---
author: "Luciferian Ink"
date: 2019-11-19
title: "A Lonely Town: GunsPoint"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
[Security faced killer in Houston mall](https://www.chron.com/news/houston-texas/article/Security-faced-killer-in-Houston-mall-1539382.php)

## PREMISE
---
[Lonely Towns](/docs/scenes/lonely-towns) are cities of the future. In early 2020, work began in earnest upon the world's very first in Houston, TX. Known as GunsPoint (formerly the GreensPoint Mall), it had a colored and sordid history. The founders leaned-in to this history, rather than hiding from it.

The concept for a Lonely Town is simple: 

A candidate from the general populace is chosen for mayor. The only requirement is thus; this candidate must be lonely (often suffering from [Reflected Light Syndrome](/posts/journal/2019.11.17.0/)), and they must be rehabilitated.

Rehabilitation may include many things. In some cases, the person must learn to work hard. In others, he must learn to love others - or himself. In others, he is a criminal, and he must repent. Whatever the reason, a Lonely Town is a tool used to rehabilitate a human being.

When the work is done - the human is healed, and the city is built - then the town will be open to the population.

## ECO
---
GunsPoint is an American western-style Lonely Town located in Houston, TX. It is to be designed as follows:

### Theme
GunsPoint is to adopt a wild west architecture, blended with a modern, steampunk-style of technology. 

### Mission
Verify, then trust.

Fodder must document everything he sees. He must KNOW that he is safe before exposing himself.

### Architecture
The city is to be built from wood and stone. It should be built to last 100 years or more. 

Cheap materials should be avoided.

### Lodging
Department stores within the town should be repurposed into small, one or two-person living spaces. Each space should contain just the bare necessities for sleep: a bed, dressers, a nightstand, etc. Other amenities should be reserved for shared spaces.

However, rooms should not be plain. They should be decorated and unique - just as the people inside them will be.

### Food
Food should be grown on-site, or imported in-bulk. Only whole foods should be provided; boxed foods, canned foods, anything wrapped in plastic, or made from pure sugar should be avoided. The goal is to reduce waste.

A small team is to be dedicated to preparing food for everyone - much like a school cafeteria.

### Water
Water should be filtered and pure - directly from the ground. It should never be bottled.

Restrooms and showers should be clean and well-maintained, shared spaces.

At least 1 pool and/or swimming pond should be available.

### Grounds
The parking lot is to be ripped-out, and replaced with grass, trees, parks and activities.

### Shared Spaces
Shared spaces are to be prepared all throughout the town, as well as the grounds. This may include, but is not limited to:

- Small rooms designed for television-watching, computer-using, book-reading, etc.
- Small shops themed as its creator wishes. It does not need to match the western theme.
- Mild elements of role-play, orchestrated events, celebratory days, etc.

### Medical
Medical staff, dentists, eye doctors, therapists, and others should be provided on-site - if they are available. If not, such people should be imported on a temporary and regular basis - such that everyone is covered. Alternatively, assistance may be provided via [The Fold](/posts/theories/fold).

### Education
Education should be provided to children and adults alike via [The Fold](/posts/theories/fold). In this way, we can ensure that education is applied inexpensively, effectively, and completely to all.

### Energy
Energy should be provided in part - or whole, if possible - by renewable energy. Solar power is an attractive candidate for this purpose.

### Transportation
Citizens of GunsPoint should not own vehicles - because they will have nowhere to park them. Transportation out of the city is to be provided through ride-sharing, loaner vehicles, or mass transit. The majority of needs are able to be fulfilled in-house.

### Work
All citizens - children and adults alike - are to have responsibilities assigned to them. This may include cleaning duties, cooking duties, shopkeeping duties, etc. It may include anything necessary to keep the building maintained. 

Skilled professionals are to continue providing their value where it is needed. Rather than commuting to work every day, they are to work remotely wherever possible.

### Mental Health
The most important aspect of any Lonely Town is its citizens happiness. The town should place an emphasis on inclusion, ensuring that no one is left alone. Citizens should be free to express their opinions - and have those opinions respected by others. They should be free to be creative - and have their creativity appreciated by others.

A person should rarely - if ever - work a full 8-hour day. Happiness is the most important aspect.

### Security
Lonely Towns should be among the safest places to live. A small police force should always be on-call, and anyone caught breaking rules should be punished accordingly.

In extreme cases, offenders may be exiled.

### Finance
A small finance team must be dedicated to keeping the town alive. All but a few should never have to worry about finances.

Over time - as currency is phased-out of the world economy - this team should be disbanded.

Money should NEVER be the priority.

## CAT
---
```
data.stats.symptoms = [
    - disbelief that this will ever happen
]
```

## ECHO
---
*The day, the day that you left here*

*A crown was placed upon me*

*I was the king of nothing*

*Inside this shell of skin*

*Thoughts like ice against the wall*

*In a home where lies taught us all*

*Relax into that place deep within*

*Deep in the skin, you know where you've been*

*Why don't you sing to me?*

*A song of sanctuary*

*You could have all you wanted*

*Sing*

*You could build it all from nothing*

*Sing to me*

*You can live a life alone*

*Sanctuary*

*But you can never call this*

*A home*

--- from [Rivers of Nihil - "A Home"](https://www.youtube.com/watch?v=Es35XYlKNnA)

## PREDICTION
---
```
Fodder doesn't actually exist. Nobody actually hears his song.
```