---
author: "Luciferian Ink"
date: 2019-11-19
title: "A Lonely Town: Lullaby City"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
[The Inventor's](/docs/confidants/inventor) creation.

## ECO
---
Lullaby City (formerly Houston, TX) is a humble town, maintained wholly by its wealthy and empathetic benefactors. It is the world's first [Lonely Town](/docs/scenes/lonely-towns) of such scale - and it will be the first to abolish currency.

### Theme
Lullaby City is to be themed as the Inventor wishes. Context tells us that she will choose a modern steampunk style - complete with UFOs, aliens, robots, and contraptions.

Ink politely requests that she integrate music - specifically metal - into her design. All other genres are welcome, too.

### Architecture
Lullaby City should be constructed from humble beginnings. Two individuals - a father and a daughter - are to be given a home in exchange for their labor. 

The two are to live in darkness - separated from each other, never receiving [reflected light](/posts/journal/2019.11.17.0/) - until the day that they complete the construction of this home.

Perched upon the highest towers of the city should be massive spotlights. They should be used to keep the evil Blixies at bay - and to highlight the good ones.

### Mission
Keep doing exactly what you are doing.

I am coming to save you.

### Other
Provide her with whatever she needs. 

Do not force her to do this alone. Provide her with guidance.

## CAT
---
```
data.stats.symptoms = [
    - empathy
]
```

## ECHO
---
*Instrumental*

--- from [Ne Obliviscaris - "Painters of the Tempest (Part III): Reveries from the Stained Glass Womb"](https://www.youtube.com/watch?v=FQRIqIlhapQ)

## PREDICTION
---
```
The Inventor will be instrumental to the creation of this city.
```