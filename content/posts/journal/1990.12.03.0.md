---
author: "Luciferian Ink"
date: 1990-12-03
title: "Near Miss"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## ECO
---
It was a dark, rainy evening as [Fodder](/docs/personas/fodder) sat in the back of his parent's car. They had forgotten to buckle him into his seat.

They were traveling through an empty parking lot when Fodder opened the rear door, and fell onto the pavement.

He was immediately taken to the hospital. He had minor scrapes and bruises.

There was no apparent damage to his head.

## CAT
---
```
data.stats.symptoms = [
    - fear
    - pain
]
```

## ECHO
---
*No, I don't know where I'm goin'*

*But I sure know where I've been*

*Hanging on the promises in songs of yesterday*

*And I've made up my mind*

*I ain't wasting no more time*

*And here I go again*

--- from [Whitesnake - "Here I Go Again"](https://www.youtube.com/watch?v=WyF8RHM1OCg)