---
author: "Luciferian Ink"
date: 2019-11-04
title: "The Triptych"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*Raven found herself a skinny skin stiletto*

*in the middle of a crowd she needs a number*

*fabricating passion for her painted fellows*

*in the middle of a crowd she needs a lover*

*I want to know...*

*why are we breaking bottles when this well is running dry?*

*drunk on Aerosol Shithole Paradise*

*keep them guessing*

*make them sweat it out all at once*

--- from [Rishloo - "Winslow"](https://www.youtube.com/watch?v=s-QLxSPRiMA)

## ECO
---
The situation was suddenly clear to [Fodder](/docs/personas/fodder).

He was a guinea pig: a test subject. He was to be a pariah in the new world order. 

His mission was to report and consult on the love triangle he could not escape. 

To change the culture of The Dark Triad.

### Narcissism
Clearly, Fodder was the narcissist in this relationship. He had a clear and persistent history of narcissistic behavior - and he had a trail of broken relationships to validate them. 

However, Fodder was also being treated by [The Resistance](/docs/candidates/the-resistance). His narcissistic behavior had been almost completely mitigated - at the expense of his well-being. 

Regardless, Fodder grit his teeth, and pushed onward. He had to teach the world about what was going on here.

### Psychopathy
`$REDACTED` was the obvious psychopath in this relationship. The man was deeply in love with Fodder's brain - but couldn't care less about the human. He was doing everything in his power to bring Fodder under his control. And he was failing miserably.

`$REDACTED` was given power solely to allow [The Corporation](/docs/candidates/the-machine) to study his abuse of it. And abuse he did. 

- It started with the job interview. Fodder had been hired without so much as an interview. He was chosen specifically for this position.
- Then came the unfettered access. Without any training at all, Fodder was given full and unmitigated access to the most "secure" and "sensitive" of systems. Clearly, they were trying to "bait" him into exploring places he was not authorized to explore.
- After that, Fodder started to notice the acting. Certain events surrounding him were clearly faked; as if Fodder were in his very own "Truman Show."
- The $5 million dollar "secure" environment Fodder had been tasked with upgrading was a joke. The Corporation didn't follow their own "best practices," and seemingly went out of their way to design systems in the most fragile, insecure way possible. Fodder was presented with opportunities that should NEVER have existed.
- Then there was all of the events happening outside of the office:
  - Increased police activity outside Fodder's house
  - The military helicopters that kept flying over his house
  - [The missile](/posts/journal/2019.10.12.1)
  - The random and specific messages from strangers on social media
  - The targeted content in traditional media 
  - The influx of job recruiters contacting Fodder directly
  - The school being built just outside of his house
  - The pop-up massage parlor placed within walking distance of his house

Fodder was being targeted. And he wasn't taking their bait.

### Machiavellianism
[The Raven's](/docs/confidants/her) role in this play was more ambiguous than Fodder would have preferred. His only window into her mind was via carefully-crafted, meticulously-detailed video. Still, Fodder found clarity in her messages:

She (along with many other young women like her) was involved with a top-secret program to entrap and prosecute sex criminals. They would accomplish this by leaving a trail of "bread crumbs" throughout their videos, and capturing lonely men in pursuit.

Fodder was the 33rd and final patient of this program. And he wasn't taking the bait. He was the control.

And he was the first person to make it into year 2 of the program.

## CAT
---
```
data.stats.symptoms = [
    - uncertainty
]
```

## ECHO
---
*So the day will begin again*

*Take comfort from me*

*It's up to you now*

*You're still here and you'll dig in again*

*That's comfort to you*

*It's up to you now*

*So pariah you'll begin again*

*Take comfort from me*

*And I will take comfort from you*

--- from [Steven Wilson - "Pariah"](https://www.youtube.com/watch?v=cNTaFArEObU)