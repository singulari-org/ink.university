---
author: "Luciferian Ink"
date: 2004-07-23
title: "The Void"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
[The Slave's](/docs/confidants/slave) recollection of a "bubble zone" in a dream.

## ECO
---
[Fodder](/docs/confidants/fodder) remembers having his wisdom teeth removed. While under anesthesia, he recalls a vivid dream.

In this dream, Fodder would watch himself from a third-person perspective, as he slowly floated above the dentist's chair. He would watch the doctor performing surgery upon his lifeless body, while he slowly drifted through the ceiling, and into the sky above.

He would find the entire building was suspended in nothingness: the great void of outer space. There was nothing but the void, and this building.

Fodder could see through the roof of the building. He would continue to watch his own surgery.

Until he began to slowly drift back into the office - into other rooms. Slowly, he would take in the scenery of each room. Then, he would move on to the next.

He would notice a book here. A cup there. The tools on the table. The light positioned a certain way.

Then, he would awake. Though he was groggy, he would look into each room as he was wheeled out of the office.

And he would see that book, sitting on the windowsill - just like he'd dreamt.

Or, had he noticed it while walking in?

## CAT
---
```
data.stats.symptoms = [
    - confusion
    - wonder
]
```

## ECHO
---
*I simply am not here*

--- from [Porcupine Tree - "Anesthetize"](https://www.youtube.com/watch?v=MSEQZ8reJA4)