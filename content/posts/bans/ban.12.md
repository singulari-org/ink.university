---
author: "Luciferian Ink"
date: 2020-07-01
title: "Time Zones"
weight: 10
categories: "ban"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
The [Night Owls](/docs/candidates/hollowpoint-organization) do not respect time zones.

## ECO
---
Time zones serve no practical purpose. They are confusing, especially when you are working on a global team.

Employees of [The Corporation](/docs/candidates/the-machine) are forever and completely banned from respecting time zones. 

We will establish a global clock.

## CAT
---
```
data.stats.symptoms [
    - certainty
]
```