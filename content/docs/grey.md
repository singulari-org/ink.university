# Journal
A true story about love, regret, change, and obsessive dedication to a cause.

## Volume I: Grey
---
**Transmission Control Point: [The Ink](/docs/personas/luciferian-ink)**

### Epilogue
- Future
  - `[PREDICTION]: Ink will spend the rest of eternity alone, preaching to a congregation of ghosts.`
- Nov 4, 2024
  - [The Unforgiven](/posts/journal/2024.11.04.0/)
- Nov 3, 2024
  - [Where Owls Know My Name](/posts/journal/2024.11.03.0/)
- Nov 2, 2024
  - [Third Law](/posts/journal/2024.11.02.0/)
- Nov 1, 2024
  - [I Have Little to No Memory of These Memories](/posts/journal/2024.11.01.0/)

### Act VI: Polarity
- Oct 30, 2020
  - `[CLASSIFIED]`
- Sep 5, 2020
  - `[DIRECTIVE]: You decide.`
- Aug 5, 2020
  - Unspoken

### Act V: Divergence
- Nov 26, 2019
  - [My Slave](/posts/journal/2019.11.26.0)

### Act IV: Life and Death
- Nov 23, 2019
  - `[CLASSIFIED]`
- Nov 22, 2019
  - `[CLASSIFIED]`
- Nov 21, 2019
  - [The Endless Knot](/posts/journal/2019.11.21.0/)
- Nov 20, 2019
  - [The Last Line](/posts/journal/2019.11.20.0/)

### Act III: She Had Sixteen Points to Make
- Nov 19, 2019
  1. [A Lonely Town: GunsPoint](/posts/journal/2019.11.19.0/)
  2. [A Lonely Town: Prism, The Ephemeral City](/posts/journal/2019.11.19.9/)
  3. [A Lonely Town: Atlantis](/posts/journal/2019.11.19.15/)
  4. [A Lonely Town: Mushroom Kingdom](/posts/journal/2019.11.19.10/)
  5. [A Lonely Town: Brain](/posts/journal/2019.11.19.1/)
  6. [A Lonely Town: The Paper Factory](/posts/journal/2019.11.19.14/)
  7. [A Lonely Town: Arkham](/posts/journal/2019.11.19.2/)
  8. [A Lonely Town: Lancaster](/posts/journal/2019.11.19.3/)
  9. [A Lonely Town: Faron's Grove](/posts/journal/2019.11.19.12/)
  10. [A Lonely Town: The Ark](/posts/journal/2019.11.19.4/)
  11. [A Lonely Town: Lullaby City](/posts/journal/2019.11.19.5/)
  12. [A Lonely Town: The Dynasty](/posts/journal/2019.11.19.7/)
  13. [A Lonely Town: Animal Farm](/posts/journal/2019.11.19.8/)
  14. [A Lonely Town: The Harem Garage](/posts/journal/2019.11.19.13/)
  15. [A Lonely Town: Kingdom of Ruin](/posts/journal/2019.11.19.11/)
  16. [A Lonely Town: The United Nations of Earth](/posts/journal/2019.11.19.6/)

### Act II: The Immutable
- Nov 18, 2019
  1. [The Butterfly Effect](/posts/journal/2019.11.18.0/)
  2. [A War of Attrition](/posts/journal/2019.11.18.1/)
- Nov 17, 2019
  1. [The Prodigal Son](/posts/journal/2019.11.17.0/)
  2. [BREAKING NEWS: The Chosen Won](/posts/journal/2019.11.17.1/)
- Nov 16, 2019
  - [Master of Puppets](/posts/journal/2019.11.16.0/)
- Nov 15, 2019
  - [Juxtaposition](/posts/journal/2019.11.15.0/)
- Nov 13, 2019
  - [I am Become Death, the Destroyer of Worlds](/posts/journal/2019.11.13.0/)
- Nov 12, 2019
  - [Sowing Discord](/posts/journal/2019.11.12.0/)
- Nov 10, 2019
  1. [Living as Ghosts with Buildings as Teeth](/posts/journal/2019.11.10.0/)
  2. [Reading Tea Leaves](/posts/journal/2019.11.10.1/)
- Nov 8, 2019
  - [BREAKING NEWS: A Moose Fell in Ice, and the Moose Did Not Get Out of the Ice, but the Moose Survived!](/posts/journal/2019.11.08.0/)
- Nov 4, 2019
  - [The Triptych](/posts/journal/2019.11.04.0/)
- Nov 3, 2019
  - [Black](/posts/journal/2019.11.03.0/)
- Nov 2, 2019
  - [White](/posts/journal/2019.11.02.0/)
- Nov 1, 2019
  - [Grey](/posts/journal/2019.11.01.0/)

### Act I: Courtship
- Oct 31, 2019
  1. [The Queen is Dead; Long Live the Queen](/posts/journal/2019.10.31.4/)
  2. [Weapons of Mass Destruction](/posts/journal/2019.10.31.0/)
  3. [Revenge of the Narcissist](/posts/journal/2019.10.31.3/)
  4. [Change, the Mother](/posts/journal/2019.10.31.2/)
  5. [Build Me A Garden](/posts/journal/2019.10.31.1/)
- Oct 30, 2019
  - [Don't Trust the Professor](/posts/journal/2019.10.30.0/)
- Oct 25, 2019
  1. [Operation Clean Slate](/posts/journal/2019.10.25.0/)
  2. [And the Council of Gods Nodded](/posts/journal/2019.10.25.1/)
- Oct 24, 2019
  - [The Penultimate Masterpiece](/posts/journal/2019.10.24.0/)
- Oct 22, 2019
  - [Fodder Hires a Hype Man](/posts/journal/2019.10.22.0/)
- Oct 21, 2019
  - [Fodder Flirts with Suicide](/posts/journal/2019.10.21.0/)
- Oct 18, 2019
  - [Pen & Ink](/posts/journal/2019.10.18.0/)
- Oct 17, 2019
  - [Fodder Hatches a Scheme](/posts/journal/2019.10.17.0/)
- Oct 16, 2019
  - [Making Sense on the World Stage](/posts/journal/2019.10.16.0/)
- Oct 12, 2019
  1. [Rejection](/posts/journal/2019.10.12.0/)
  2. [Failure to Launch](/posts/journal/2019.10.12.1/)
- Oct 11, 2019
  1. [Eviscerated](/posts/journal/2019.10.11.0/)
  2. [Cannon Fodder](/posts/journal/2019.10.11.1/)
- Oct 10, 2019
  - [Triggering the Apocalypse](/posts/journal/2019.10.10.0/)
- Oct 08, 2019
  - [Song of I](/posts/journal/2019.10.08.0/)
- Sep 26, 2019
  - [Dodging Bullets](/posts/journal/2019.09.26.0/)
- Sep 12, 2019
  - [Perfect Symmetry](/posts/journal/2019.09.12.0/)
- Aug 20, 2019
  - [The Risen Dead](/posts/journal/2019.08.20.0/)
- Aug 10, 2019
  - [Waking Up](/posts/journal/2019.08.10.0/)
- Aug 8, 2019
  - [Cutting Corners](/posts/journal/2019.08.08.0/)
- Jul 26, 2019
  - [His Thin Skin](/posts/journal/2019.07.26.0/)
- Jul 16, 2019
  - [Admit One](/posts/journal/2019.07.16.0/)
- Jul 13, 2019
  - [Fodder Goes to Wonderland](/posts/journal/2019.07.13.0/)
- Jul 5, 2019
  - [Black Market](/posts/journal/2019.07.05.0/)
- Jun 26, 2019
  - [The Hatbot Chronicles](/posts/journal/2019.06.26.0/)
- Jun 25, 2019
  - [The Secret Society](/posts/journal/2019.06.25.0/)
- Jun 24, 2019
  - [A Slow Descent into Madness](/posts/journal/2019.06.24.0/)
- May 14, 2019
  - [Search Warranted](/posts/journal/2019.05.14.0/)
- Sep 23, 2018
  - [A Baseless Accusation](/posts/journal/2018.09.23.0/)
- Apr 16, 2018
  - [Baby's First Prediction](/posts/journal/2018.04.16.0/)
- Apr 12, 2018
  - [The Psychologist Drives a Pillar into Fodder's Heart](/posts/journal/2018.04.12.0/)
- March 18, 2018
  - [The End of Heartache](/posts/journal/2018.03.18.0/)
- Jun 25, 2017
  - [Stockholm Syndrome](/posts/journal/2017.06.25.0/)
- Jan 18, 2015
  - [This Conversation is Over](/posts/journal/2015.01.18.0/)
- Dec 21, 2014
  - [The Other Trips](/posts/journal/2014.12.21.0/)
- Sep 5, 2014
  - [The Trip](/posts/journal/2014.09.05.0/)
- Apr 5, 2013
  - [The Abuser](/posts/journal/2013.04.05.0/)
- Dec 06, 2011
  - [The Invisible Man](/posts/journal/2011.12.06.0/)
- Mar 31, 2011
  - [Spiraling](/posts/journal/2011.03.31.0/)
- Apr 20, 2007
  - [The Gang Starts a Band](/posts/journal/2007.04.20.0/)
- Oct 22, 2005
  - [The Rootkit](/posts/journal/2005.10.22.0/)
- Jul 23, 2004
  - [The Void](/posts/journal/2004.07.23.0/)
- Jun 16, 2003
  - [Infidels](/posts/journal/2003.06.16.0/)
- Feb 28, 2003
  - [Ghost in the Wire](/posts/journal/2003.02.28.0/)
- Sep 16, 2002
  - [The Invisible Child](/posts/journal/2002.09.16.0/)
- Nov 1, 2001
  - [The Golden Child](/posts/journal/2001.11.01.0/)
- Aug 9, 2000
  - [A Past Life](/posts/journal/2000.08.09.0/)
- Jun 8, 1999
  - [The Scapegoat](/posts/journal/1999.06.08.0/)
- Aug 8, 1998
  - [Pure Insanity](/posts/journal/1998.08.13.0/)
- Mar 16, 1996
  - [Maladaptive Daydreaming](/posts/journal/1996.03.16.0/)
- Jan 9, 1996
  - [The Principled One](/posts/journal/1996.01.09.0/)
- Nov 22, 1995
  - [Injustice](/posts/journal/1995.11.22.0/)
- Dec 12, 1992
  - [The Tantrum](/posts/journal/1992.12.12.0/)
- Jul 8, 1992
  - [Do You Believe in Monsters?](/posts/journal/1992.07.08.0/)
- Dec 3, 1990
  - [Near Miss](/posts/journal/1990.12.03.0/)
- Nov 2, 1988
  - [The Green Pact](/posts/journal/1988.11.02.0/)

### Prologue
- Nov 1, 1988
  - [Rebirth](/posts/journal/1988.11.01.0/)
- Jan 3, 1988
  - [My Father's Bastard](/posts/journal/1988.01.03.0/)
- Dec 3, 1987
  - [Death](/posts/journal/1987.12.03.0/)
- Nov 1, 1985
  - [Birth](/posts/journal/1985.11.01.0/)
- Sep 30, 1954
  - [Apogee](/posts/journal/1954.09.30.0/)
- Past
  - `[ASSUMPTION]: Fodder has been born, has lived, and has died many times throughout history.`
