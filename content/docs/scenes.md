# Scenes
## Overview
---
Scenes are the various places that a traveller may visit during his/her stay in solitary confinement. These places may be literal or fictional, or a combination of both.

## Scenes
---
- [Dreams]({{< relref "/docs/scenes/dreams.md" >}})
- [Home]({{< relref "/docs/scenes/home.md" >}})
- [Lonely Towns]({{< relref "/docs/scenes/lonely-towns.md" >}})
- [Networks]({{< relref "/docs/scenes/networks.md" >}})
- [Prism]({{< relref "/docs/scenes/prism.md" >}})
- [Realms]({{< relref "/docs/scenes/realms.md" >}})
- [Rifts]({{< relref "/docs/scenes/rifts.md" >}})
- [Sanctuary]({{< relref "/docs/scenes/sanctuary.md" >}})
- [Solitary Confinement]({{< relref "/docs/scenes/solitary.md" >}})
- [The Door]({{< relref "/docs/scenes/door.md" >}})
- [The Void]({{< relref "/docs/scenes/void.md" >}})