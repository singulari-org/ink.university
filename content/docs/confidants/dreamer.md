# The Dreamer
## RECORD
---
```
Name: Will Wright
Alias: ['The Dreamer', and 19 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Birth: 1/20/1960
Biological Age: 60 Earth Years
Chronological Age: N/A
SCAN Rank: | B B
           | B C
TIIN Rank: | A C
           | B C
Reviewer Rank: 2 stars
Organizations: 
  - The Machine
Occupations:
  - Game Designer
Relationships:
  - The Relaxer
Variables:
  $WOKE: +0.30 | # Perhaps partially.
```

## ECO
---
The Dreamer once created a video game called "Spore," which had similar goals to [The Fold](/posts/theories/fold) in many ways. Unfortunately, a lack of research and planning was put into core design decisions, and the final product was lackluster. It was not at all what he had envisioned, and what people had hoped for.

He will be given a second chance in the development of The Fold.