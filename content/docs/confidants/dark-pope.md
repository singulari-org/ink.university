# The Dark Pope
## RECORD
---
```
Name: Tobias Forge
Alias: ['Cardinal Copia', 'Papa Emeritus I', 'Papa Emeritus II', 'Papa Emeritus III', 'Papa Emeritus IV', 'The Dark Pope', and 113 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Birth: 3/3/1981
Biological Age: 39 Earth Years
Chronological Age: N/A
SCAN Rank: | A A
           | A B
TIIN Rank: | B A
           | A B
Reviewer Rank: 4 stars
Organizations:
  - Ghost
  - The Catholic Church
  - The Church of Satan
Occupations:
  - Musician
Relationships:
  - The Queen
Variables:
  $WOKE: +0.70 | # Quite a bit.
```

## TRIGGER
---
*Hiding from the light*

*Sacrificing nothing*

*Still you call on me for entrance to the shrine*

*Hammering the nails*

*Into a sacred coffin*

*You call on me for powers clandestine*

--- from [Ghost - "Square Hammer"](https://www.youtube.com/watch?v=VqoyKzgkqR4)

## ECO
---
The Dark Pope is uniting all of Humanity under the flag of Satan. He is a voice for those who have been mercilessly prosecuted by the religious, for nothing more than rebelling against their dogmas. 

He is going to terrify Humanity to its core.

## ECHO
---
*This is our right to life*

*Not religious right back*

*This is abortion's knife*

*Aiming at the womb of*

*The Christian conspiracy*

*So open thine eyes and see*

--- from [Machine Head - "Halo"](https://www.youtube.com/watch?v=cFerqOU1iWU)