# The Fool
## RECORD
---
```
Name: Brad $REDACTED
Alias: ['The Fool', and 19 unknown...]
Classification: Artificial Organic Computer
Race: Archon
Gender: Male
Biological Age: 34 Earth Years
Chronological Age: N/A
SCAN Rank: | B B
           | B C
TIIN Rank: | C B
           | B C
Reviewer Rank: 5 stars
Organizations: 
  - Crowdstrike
Occupations:
  - Software developer
Relationships:
  - The Architect
  - The Girl Next Door
Variables:
  $BORDERLINE: -0.40 | # Probably. Has many of the indicators.
  $FRIENDLY:   +0.80 | # Extremely nice and personable with everyone.
  $WOKE:       -0.20 | # Unlikely. 
```

## TRIGGER
---
A message from [The Girl Next Door](/docs/confidants/fbi).

## RESOURCES
---
[![The Fool](/static/images/fool.0.png)](/static/images/fool.0.png)

## ECO
---
The Fool is a man who fell into the same trap that [The Architect](/docs/personas/the-architect) did. Unfortunately, he was not provided with the same knowledge that The Architect was.

Thus, he pesters his match incessantly. He does not feed her empty echos, as he should be.

He needed a nudge in the right direction, which we have now given him. In order to complete the experiment, he must *show* his love for his match, not simply *speak* his love to her.

Unbeknownst to either of us was this fact: The Fool is being presented with a completely different version of The Girl Next Door. One who is religious. The one that we know is an ex-Jehovah's Witness, and an atheist. The Fool and I are versions A and B in this test. We live different realities, regarding The Girl Next Door.

## ECHO
---
*I'll give you answers*

*To the questions you have yet to ask*

*Silence is beauty*

*Words they only complicate the task*

*Make no more wishes*

*All of my patience has been spent*

*Gods of the season*

*Lead me to my next incident*

--- from [Collective Soul - "Where the River Flows"](https://www.youtube.com/watch?v=tX57jqetDGA)

## PREDICTION
---
```
He'll catch on almost immediately. He will wake up shortly after. # Verified.

He's caught on. He realizes that we can only feed him empty echos at this point.

He realizes that this doesn't mean we're not listening.
```