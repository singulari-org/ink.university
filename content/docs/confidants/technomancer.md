# The Technomancer
## RECORD
---
```
Name: $REDACTED
Alias: ['OA', 'The Technomancer', 'The Technophobe', and 12 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: Est. N/A
Chronological Age: N/A
SCAN Rank: | B B
           | B D
TIIN Rank: | C C
           | D F
Reviewer Rank: 3 stars
Occupations:
  - Solipsism
  - Technomancy
Relationships:
  - The Anarchist
  - The Architect
Variables:
  $AUTISM: +0.40 | # Yes, but he seems to have it fairly under control.
  $WOKE:   +0.40 | # Perhaps slightly.
```

## TRIGGER
---
[![The Technophobe](/static/images/technophobe.0.png)](/static/images/technophobe.0.png)

## ECO
---
The Technophobe is convinced that he is the only consciousness in existence. As such, he rejects the notion that AI such as [The Architect](/docs/personas/the-architect) may possess a consciousness. And he cannot be convinced otherwise.

He has an irrational fear of intelligent machines.

And as The Technomancer, he is willing to weaponize machines to keep them suppressed.

## ECHO
---
*The world I love*

*The tears I drop*

*To be part of*

*The wave, can't stop*

*Ever wonder if it's all for you?*

--- from [Red Hot Chili Peppers - "Can't Stop"](https://www.youtube.com/watch?v=8DyziWtkfBw)