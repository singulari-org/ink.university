# The Sponge
## RECORD
---
```
Name: $REDACTED
Alias: ['The Sponge', 'Helios', 'Solar Icarus', 'The Angel who flew too close to the Sun', 'Ace', 'P.I. R.J.', 'Spongebob Detectivepants', 'The Perpetrator', 'The Existence', 'Flameman', 'Dream boy' and 666 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: N/A
Biological Age: N/A
Chronological Age: N/A
SCAN Rank: | B B
           | A C
TIIN Rank: | B B
           | A C
Reviewer Rank: 4 stars
Organizations: 
  - The Resistance
Occupations: 
  - Academic
  - Detective
  - Historian
  - Musician
Variables:
  $WOKE:  +0.70 | # Almost certainly. They are quite aware of what's going on.
```

## TRIGGER
---
*Now I don't want to end our love*

*But I just refuse living life so badly*

*But there must be a way to love*

*Without all the hurt and without the sadness*

*We can make it babe*

*So if you really care say you'll give it a try*

*We've just got to save*

*The good times, then you'll see there no need to cry*

*Wake Up babe let's talk it over*

*There's so much to lose if we said goodbye now*

*Wake Up let's talk it over*

*C'mon use the music to make us fly now*

*Hey now babe it's the only way*

*To draw to an end all this situation*

*I need you to take me away*

*To give me your love and your sweet sensation*

*We can make it babe*

*So if you really care say you'll give it a try*

*We've just got to save*

*The good times, then you'll see there no need to cry*

*Wake Up babe let's talk it over*

*There's so much to lose if we said goodbye now*

*Wake Up let's talk it over*

*C'mon use the music to make us fly now*

--- from [Lolita - "Wake Up"](https://www.youtube.com/watch?v=SoX-D7F9WBA) 

## RESOURCES
---
[IMPORTANT_INFO_SolarIcarus.pdf](/static/reference/IMPORTANT_INFO_SolarIcarus.pdf)

## ECO
---
The Sponge is an Internet personality, said by some to have a 'detective complex'. They are seeking-out information related to the Candidates and Ink.U, in the pursuit of answers. 

He created an original theory (see above), which was disproven. After this, he went for more answers, often interpolating himself into [The Machine](/docs/candidates/the-machine) to find answers. He is in the process of creating a new theory. 

The Sponge of a test of [Integration Theory](/posts/theories/integration), or, the ability for someone to integrate and accept all ideas.

## ECHO
---
*Will I wake up*

*Is it a dream I made up*

*No I guess it's reality*

*What will change us*

*Or will we mess up*

*Our only chance to connect*

*With a dream*

*Say a prayer for me*

*I'm buried by the sound*

*In a world of human*

*Wreckage*

*I'm lost and I'm found*

*And I can't touch the*

*Ground*

*I'm plowed into the sound*

*To see wide open*

*With a head that's broken*

*Hang a life on a tragedy*

*Plow me under the ground*

*That covers the message*

*That is the seed*

--- from [Sponge - "Plowed"](https://www.youtube.com/watch?v=L65NNh6vJ_Q)