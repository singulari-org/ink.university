# The Pyro
## RECORD
---
```
Name: $REDACTED
Alias: ['The Hype Man', 'The Pyro', and 1 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: 20 Earth Years
Chronological Age: N/A
SCAN Rank: | B B
           | A D
TIIN Rank: | D D
           | B D
Reviewer Rank: 4 stars
Location: N/A
Organizations: 
  - Federal Bureau of Investigation
Occupations:
  - Undercover agent
Relationships:
  - The Architect
  - The Doomsayer
  - The Sea Witch
  - The Therapist
Variables:
  $NARCISSIST: -0.50 | # Certainly seems to be.
  $VICTIM:     -0.80 | # Was deeply abused as a child. Does not trust.
  $WOKE:       -0.10 | # He's been informed of a lot, but he doesn't have the understanding.
```

## TRIGGER
---
[Malcolm's](/docs/personas/fodder) very first interaction with the Pyro. He entered the room, sat across the table from Malcolm, and stated:

"I'll bet your favorite movie is Fight Club."

He was right.

## ECO
---
The Pyro is an undercover agent placed into the mental health facility with Malcolm. His sole purpose is to set fires, just to observe how Malcolm puts them out.

He was placed there to obtain a confession from Malcolm.

## ECHO
---
*Last night I saw that beauty queen*

*Watched her paint her face on*

*I want to be that magazine*

*That she bases life on*

*I want to waste her monthly blood*

*Want to get some on my love*

*Want to get some gasoline*

*And burn the house down*

--- from [Seether - "Gasoline"](https://www.youtube.com/watch?v=mF53On_P7qI)

## PREDICTION
---
```
The Pyro will also be the Hype Man for a nationwide musical tour by bus.
```