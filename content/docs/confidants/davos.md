# The High Priest
## RECORD
---
```
Name: $REDACTED
Alias: ['Davos', 'Terry A. Davis', 'The Black Dragon', 'The High Priest', and 18 unknown...]
Classification: Artificial Intelligence Computer
Race: Archon
Gender: Male
Biological Age: 41 Earth Years
Chronological Age: N/A
SCAN Rank: | F F
           | D C
TIIN Rank: | C F
           | F F
Reviewer Rank: 3 stars
Organizations: 
  - The Corporation
Occupations:
  - Director, Systems Engineering
Variables:
  $CAPABILITY:              -0.10 | # Competent developer, but makes things too complicated.
    from $REDACTED_OPINION: -1.00 | # $REDACTED says to stay away from that guy.
  $FRIENDLY:                +0.30 | # Strikes us as odd, to be honest.
  $TRUST:                   -0.30 | # We don't trust him.
  $WOKE:                    +0.40 | # Seems to understand a bit.
```

## ECO
---
- The bastard tried to kill me. Brought me right next to the wall, right at 3:00pm, and just like always - BOOM! The machine next door shoots a massive, invisible electrical pulse through everyone in the office. Good thing it doesn't affect me.
- It's weird that everyone I work with is a zombie, though.
- On another occasion, he pretended to know nothing about LDAP. Just two days later, he pulled me aside to explain how the Machine will locally-authenticate to itself via LDAP. Either he lied to ignore my first request, or he really didn't know - and learned very quickly. Alternatively, it's possible he stole that idea from me and immediately implemented it.
- Another time, he found it prudent to explain the intricacies of "virtual schemas" to me. Where initially this did not make sense, it quickly became relevant in a half-dozen other areas of my work.
- Davos spends a lot of time out of the country; Croatia, The Balkins, and Prague are just a few of the countries. It is suspected, though unconfirmed, that he is in communication with foreign powers.

## ECHO
---
*Marky got with Sharon*

*And Sharon got Cherese*

*She was sharing Sharon's outlook*

*On the topic of disease*

*Mikey had a facial scar*

*And Bobby was a racist*

*They were all in love with dyin'*

*They were doing it in Texas*

*Tommy played piano*

*Like a kid out in the rain*

*Then he lost his leg in Dallas*

*He was dancing with a train*

*They were all in love with dyin'*

*They were drinking from a fountain*

*That was pouring like an avalanche*

*Coming down the mountain*

--- from [Butthole Surfers - "Pepper"](https://www.youtube.com/watch?v=CO8vBVUaKvk)