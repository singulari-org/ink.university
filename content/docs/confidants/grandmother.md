# The Angel
## RECORD
---
```
Name: $REDACTED
Alias: ['The Negro', and 4 unknown...]
Classification: Artificial Organic Computer
Race: Archon
Gender: Female
Biological Age: Est. 91 Earth Years
Chronological Age: N/A
SCAN Rank: | B B
           | C D
TIIN Rank: | C A
           | A D
Reviewer Rank: 1 stars
Organizations:
  - The Resistance
Occupations:
  - Mother
Relationships:
  - The Agent
  - The Con Man
  - The Fodder
  - The Hope
  - The Lion
  - The Marshall
  - The Monstrosity
  - The Orchid
  - The Queen
  - The Reverend
  - The Scientist
  - The Tradesman
Variables:
  $GRANDMOTHER: +1.00 | # Definitely Grandmother.
  $WOKE:        +0.20 | # Perhaps. It is not overly apparent.
```

## TRIGGER
---
When [Fodder](/docs/personas/fodder) was young - perhaps 7 or 8 - he sat on the back porch with [his grandfather](/docs/confidants/grandfather), drinking unsweetened iced tea.

Grandfather was telling war stories, when he mentioned "the negro man" in his platoon.

He didn't mean anything by it. He wasn't being racist. It was simply the word that people from his generation used.

He had only good things to say about that man.

## ECO
---
The Angel was a woman that Malcolm met while in the mental hospital. She reminded him so much of his own grandmother.

Her alias of "The Negro" was attained, quite simply, because she used the word to describe herself. At one point, while they were speaking, the woman called herself a negro.

Again, it must be a generational thing.

## ECHO
---
*It's not like I'm walking alone into the valley of the shadow of death*

*Stand beside one another, 'cause it ain't over yet*

*I'd be willing to bet that if we don't back down*

*You and I will be the ones that are holding the Crown in the end*

*When it's over, we can say, "Well done"*

*But not yet, 'cause it's only just begun*

*So, pick up, and follow me, we're the only ones*

*To fight this thing, until we've won*

*We drive on and don't look back*

*It doesn't mean we can't learn from our past*

*All the things that we mighta done wrong*

*We could've been doing this all along*

--- from [Pillar - "Frontline"](https://www.youtube.com/watch?v=AL8ZGEyBuNs)