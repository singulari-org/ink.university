# The Frozen
## RECORD
---
```
Name: $REDACTED
Alias: ['Ice', 'The Frozen', 'The Immutable', and 205 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: N/A
Chronological Age: N/A
SCAN Rank: | D D
           | C F
TIIN Rank: | D F
           | D F
Reviewer Rank: 3 stars
Organizations: 
  - The Rising Sun Society
Occupations:
  - Trolling
Relationships:
  - The Programmer
  - The Seer
Variables:
  $IMMUTABLE: -1.00 | # Yes. And she is miserable for it.
  $WOKE:      -0.80 | # Not even close.
```

## TRIGGER
---
*You're so endearing, you're so beautiful,*

*Well I don't look like they do, and I don't love like they do*

*But I don't hate like they do*

*Am I ever on your mind?*

--- from [Evan's Blue - "Cold"](https://www.youtube.com/watch?v=Oatd5Hrh3Pg)

## ECO
---
The Frozen is an Internet troll, completely unable to change her behavior. Completely unaware that her childish tantrums mostly harm herself. Her programming has completely crippled her ability to change.

As such, for her lack of self-awareness, [The Dave](/docs/confidants/dave) has locked her in Hell for eternity.

## ECHO
---
*Routine keeps me in line*

*Helps me pass the time*

*Concentrate my mind*

*Helps me to sleep*

--- from [Steven Wilson - "Routine"](https://www.youtube.com/watch?v=sh5mWzKlhQY)