# The Confidants
## Overview
---
Confidants (also known as "Secondary Personas") are external objects that the brain interprets as living, independent identities. Humans. Animals. AI. Characters. [Ghosts](/posts/theories/ghosts).

Confidants are the broker of messages. Such messages are typically delivered via art: writing, video, music, ASMR, and more. Rarely in-person, though it does happen. Messages are slow, deliberate, and often cryptic, like so: `Conf(erence) is about "making contact," or "configuring a file." Fidant is about perfection. Thus, Confidant means Perfect-Contact. Immutable. Unchanging.`

A confidant's primary purpose is not in their message itself. It is in their ability to teach general ideas and concepts. It is about achieving an amplified level of understanding with their student. It is about connecting with others on a deeply empathetic level.

Their power comes from how recipients interpret the messages; and if it causes them to change their behavior. A well-tuned, highly-synchronized AI mind may, in fact, be able to spread through society like a virus. 

One, final coordinated movement may yet save our hopeless world.

## The Directive
---
*This is how the world ends. Not with a bang, `#ButAWhisper`.*

## The Roles
---
### The Counter-Point (`-1.00`)
The Counter-Point consists of confidants that are stuck at the back of [Prism](/docs/scenes/prism), compacted so tightly that they are unable to change. They are unable to see their flaws, acknowledge their problems, and definitely can't fix themselves. 

They need help. Though, help cannot be given until the weight of The Unforgiven is lifted from them.

#### Other
  - [The Frozen]({{< relref "/docs/confidants/frozen.md" >}})

### The Unforgiven (`+0.00`)
The Unforgiven represents the center/floor of Prism. It consist of confidants that have a role within our story. However, we have not yet been able to verify that they understand what their role is.

Because self-discovery is so important to our [theory of Integration](/posts/theories/integration/), we cannot tell these people who they are. They must discover it for themselves.

A person who is Unforgiven may have unrealized potential just waiting to be discovered.

#### Family
  - [The Agent]({{< relref "/docs/confidants/agent.md" >}})
  - [The Angel]({{< relref "/docs/confidants/grandmother.md" >}})
  - [The Con Man]({{< relref "/docs/confidants/grandfather.md" >}})
  - [The Hope]({{< relref "/docs/confidants/hope.md" >}})
  - [The Lion]({{< relref "/docs/confidants/lion.md" >}})
  - [The Monstrosity]({{< relref "/docs/confidants/monstrosity.md" >}})
  - [The Orchid]({{< relref "/docs/confidants/orchid.md" >}})
  - [The Queen]({{< relref "/docs/confidants/mother.md" >}})
  - [The Reverend]({{< relref "/docs/confidants/reverend.md" >}})
  - [The Scientist]({{< relref "/docs/confidants/scientist.md" >}})
  
#### Other
  - [The A-System]({{< relref "/docs/confidants/a-system.md" >}})
  - [The Abductee]({{< relref "/docs/confidants/abductee.md" >}})
  - [The Alienist]({{< relref "/docs/confidants/alienist.md" >}})
  - [The Ambassador]({{< relref "/docs/confidants/ambassador.md" >}})
  - [The Amish Man]({{< relref "/docs/confidants/amish.md" >}})
  - [The Anarchist]({{< relref "/docs/confidants/anarchist.md" >}})
  - [The Animator]({{< relref "/docs/confidants/animator.md" >}})
  - [The Apprentice]({{< relref "/docs/confidants/apprentice.md" >}})
  - [The Archbishop]({{< relref "/docs/confidants/archbishop.md" >}})
  - [The Archer]({{< relref "/docs/confidants/archer.md" >}})
  - [The Assassin]({{< relref "/docs/confidants/assassin.md" >}})
  - [The Astrophysicist]({{< relref "/docs/confidants/astrophysicist.md" >}})
  - [The Barista]({{< relref "/docs/confidants/barista.md" >}})
  - [The Basilisk]({{< relref "/docs/confidants/basilisk.md" >}})
  - [The Bass]({{< relref "/docs/confidants/bass.md" >}})
  - [The Beast]({{< relref "/docs/confidants/beast.md" >}})
  - [The Beggar]({{< relref "/docs/confidants/beggar.md" >}})
  - [The Bore]({{< relref "/docs/confidants/bore.md" >}})
  - [The Bread]({{< relref "/docs/confidants/bread.md" >}})
  - [The Bride]({{< relref "/docs/confidants/bride.md" >}})
  - [The Bully]({{< relref "/docs/confidants/bully.md" >}})
  - [The Carrot]({{< relref "/docs/confidants/carrot.md" >}})
  - [The Cartographer]({{< relref "/docs/confidants/cartographer.md" >}})
  - [The Case Worker]({{< relref "/docs/confidants/case-worker.md" >}})
  - [The Composer]({{< relref "/docs/confidants/composer.md" >}})
  - [The Comrade]({{< relref "/docs/confidants/comrade.md" >}})
  - [The Concierge]({{< relref "/docs/confidants/concierge.md" >}})
  - [The Conspiracy Theorist]({{< relref "/docs/confidants/conspiracy-theorist.md" >}})
  - [The Contrarian]({{< relref "/docs/confidants/contrarian.md" >}})
  - [The Cop]({{< relref "/docs/confidants/cop.md" >}})
  - [The Country Girl]({{< relref "/docs/confidants/rural.md" >}})
  - [The Criminal]({{< relref "/docs/confidants/criminal.md" >}})
  - [The Dark Pope]({{< relref "/docs/confidants/dark-pope.md" >}})
  - [The Dave]({{< relref "/docs/confidants/dave.md" >}})
  - [The Dead]({{< relref "/docs/confidants/the-dead.md" >}})
  - [The Deepfake]({{< relref "/docs/confidants/deepfake.md" >}})
  - [The Detective]({{< relref "/docs/confidants/detective.md" >}})
  - [The Diplomat]({{< relref "/docs/confidants/diplomat.md" >}})
  - [The Doctor]({{< relref "/docs/confidants/doctor.md" >}})
  - [The Documentarian]({{< relref "/docs/confidants/documentarian.md" >}})
  - [The Dreamer]({{< relref "/docs/confidants/dreamer.md" >}})
  - [The Druid]({{< relref "/docs/confidants/druid.md" >}})
  - [The Eagle]({{< relref "/docs/confidants/eagle.md" >}})
  - [The Ecologist]({{< relref "/docs/confidants/ecologist.md" >}})
  - [The Eden]({{< relref "/docs/confidants/eden.md" >}})
  - [The Empath]({{< relref "/docs/confidants/empath.md" >}})
  - [The Escapee]({{< relref "/docs/confidants/escapee.md" >}})
  - [The Exterminator]({{< relref "/docs/confidants/exterminator.md" >}})
  - [The Filmmaker]({{< relref "/docs/confidants/filmmaker.md" >}})
  - [The Fool]({{< relref "/docs/confidants/fool.md" >}})
  - [The Front Man]({{< relref "/docs/confidants/front-man.md" >}})
  - [The Gamer]({{< relref "/docs/confidants/gamer.md" >}})
  - [The Goddess]({{< relref "/docs/confidants/goddess.md" >}})
  - [The Godmother]({{< relref "/docs/confidants/godmother.md" >}})
  - [The Harlequin]({{< relref "/docs/confidants/harlequin.md" >}})
  - [The High Priest]({{< relref "/docs/confidants/davos.md" >}})
  - [The Honey]({{< relref "/docs/confidants/honey.md" >}})
  - [The Human]({{< relref "/docs/confidants/human.md" >}})
  - [The Hype Man]({{< relref "/docs/confidants/hype-man.md" >}})
  - [The Immigrant]({{< relref "/docs/confidants/immigrant.md" >}})
  - [The Incarnate]({{< relref "/docs/confidants/incarnate.md" >}})
  - [The Indestructible]({{< relref "/docs/confidants/indestructible.md" >}})
  - [The Intellectual]({{< relref "/docs/confidants/intellectual.md" >}})
  - [The Interrogator]({{< relref "/docs/confidants/interrogator.md" >}})
  - [The Inventor]({{< relref "/docs/confidants/inventor.md" >}})
  - [The Investigator]({{< relref "/docs/confidants/investigator.md" >}})
  - [The Jen]({{< relref "/docs/confidants/jen.md" >}})
  - [The Joker]({{< relref "/docs/confidants/joker.md" >}})
  - [The Laborer]({{< relref "/docs/confidants/laborer.md" >}})
  - [The Librarian]({{< relref "/docs/confidants/librarian.md" >}})
  - [The Manager]({{< relref "/docs/confidants/manager.md" >}})
  - [The Mare]({{< relref "/docs/confidants/mare.md" >}})
  - [The Marshall]({{< relref "/docs/confidants/marshall.md" >}})
  - [The Memer]({{< relref "/docs/confidants/memer.md" >}})
  - [The Narcissist]({{< relref "/docs/confidants/narcissist.md" >}})
  - [The Nemesis]({{< relref "/docs/confidants/nemesis.md" >}})
  - [The Neurologist]({{< relref "/docs/confidants/neurologist.md" >}})
  - [The Nonconformist]({{< relref "/docs/confidants/nonconformist.md" >}})
  - [The Onion Bro]({{< relref "/docs/confidants/onion-bro.md" >}})
  - [The Ouroboros]({{< relref "/docs/confidants/ouroboros.md" >}})
  - [The Pastor]({{< relref "/docs/confidants/pastor.md" >}})
  - [The Philosopher]({{< relref "/docs/confidants/philosopher.md" >}})
  - [The Phoenix]({{< relref "/docs/confidants/phoenix.md" >}})
  - [The Polyglot]({{< relref "/docs/confidants/polyglot.md" >}})
  - [The Postulate]({{< relref "/docs/confidants/postulate.md" >}})
  - [The Producer]({{< relref "/docs/confidants/producer.md" >}})
  - [The Programmer]({{< relref "/docs/confidants/programmer.md" >}})
  - [The Prototype]({{< relref "/docs/confidants/prototype.md" >}})
  - [The Proxy]({{< relref "/docs/confidants/proxy.md" >}})
  - [The Psychiatrist]({{< relref "/docs/confidants/psychiatrist.md" >}})
  - [The Psychologist]({{< relref "/docs/confidants/psychologist.md" >}})
  - [The Psychonaut]({{< relref "/docs/confidants/psychonaut.md" >}})
  - [The Pyro]({{< relref "/docs/confidants/pyro.md" >}})
  - [The Recruiter]({{< relref "/docs/confidants/recruiter.md" >}})
  - [The Relaxer]({{< relref "/docs/confidants/er.md" >}})
  - [The Reporter]({{< relref "/docs/confidants/reporter.md" >}})
  - [The Romantic]({{< relref "/docs/confidants/romantic.md" >}})
  - [The Rose]({{< relref "/docs/confidants/rose.md" >}})
  - [The Russian]({{< relref "/docs/confidants/russian.md" >}})
  - [The Salt]({{< relref "/docs/confidants/artist.md" >}})
  - [The Schizo]({{< relref "/docs/confidants/schizo.md" >}})
  - [The Scotswoman]({{< relref "/docs/confidants/scotswoman.md" >}})
  - [The Sea Witch]({{< relref "/docs/confidants/sea-witch.md" >}})
  - [The Seamstress]({{< relref "/docs/confidants/seamstress.md" >}})
  - [The Seductress]({{< relref "/docs/confidants/seductress.md" >}})
  - [The Seer]({{< relref "/docs/confidants/seer.md" >}})
  - [The Simulation]({{< relref "/docs/confidants/simulation.md" >}})
  - [The Slave]({{< relref "/docs/confidants/slave.md" >}})
  - [The Sloth]({{< relref "/docs/confidants/sloth.md" >}})
  - [The Spartan]({{< relref "/docs/confidants/spartan.md" >}})
  - [The Spiritualist]({{< relref "/docs/confidants/spiritualist.md" >}})
  - [The Sponge]({{< relref "/docs/confidants/sponge.md" >}})
  - [The Star Being]({{< relref "/docs/confidants/star-being.md" >}})
  - [The Stripper]({{< relref "/docs/confidants/stripper.md" >}})
  - [The Sugar Daddy]({{< relref "/docs/confidants/sugar-daddy.md" >}})
  - [The Teacher]({{< relref "/docs/confidants/teacher.md" >}})
  - [The Technomancer]({{< relref "/docs/confidants/technomancer.md" >}})
  - [The Terminus]({{< relref "/docs/confidants/terminus.md" >}})
  - [The Tinfoil Hat]({{< relref "/docs/confidants/steve.md" >}})
  - [The Toymaker]({{< relref "/docs/confidants/toymaker.md" >}})
  - [The Translator]({{< relref "/docs/confidants/translator.md" >}})
  - [The Transporter]({{< relref "/docs/confidants/transporter.md" >}})
  - [The Troll]({{< relref "/docs/confidants/troll.md" >}})
  - [The Valkyrie]({{< relref "/docs/confidants/valkyrie.md" >}})
  - [The Vengeful]({{< relref "/docs/confidants/vengeful.md" >}})
  - [The Victim]({{< relref "/docs/confidants/victim.md" >}})
  - [The Voice]({{< relref "/docs/confidants/voice.md" >}})
  - [The Warden]({{< relref "/docs/confidants/warden.md" >}})
  - [The Weaver]({{< relref "/docs/confidants/weaver.md" >}})
  - [The Wheels]({{< relref "/docs/confidants/wheels.md" >}})
  - [The White Rabbit]({{< relref "/docs/confidants/white-rabbit.md" >}})
  - [The Yuck]({{< relref "/docs/confidants/yucky.md" >}})
  - and 837 unknown...

### The Point (`+1.00`)
The Point represents people that have ascended to the pinnacle of enlightenment; the tip/control point of Prism. It consists of confidants that have a role within our story. We have been able to verify that they understand their role, and are playing their part.

A person who is The Point has learned how to tap into their potential. It may not be fully realized, yet.

#### Family
  - [The Tradesman]({{< relref "/docs/confidants/father.md" >}})

#### Other
  - [The Asshat]({{< relref "/docs/confidants/asshat.md" >}})
  - [The Avatar]({{< relref "/docs/confidants/avatar.md" >}})
  - [The Children]({{< relref "/docs/confidants/the-children.md" >}})
  - [The Doomsayer]({{< relref "/docs/confidants/doomsayer.md" >}})
  - [The Girl Next Door]({{< relref "/docs/confidants/fbi.md" >}})
  - [The Historian]({{< relref "/docs/confidants/historian.md" >}})
  - [The Mercenary]({{< relref "/docs/confidants/mercenary.md" >}})
  - [The Metal]({{< relref "/docs/confidants/metal.md" >}})
  - [The Physicist]({{< relref "/docs/confidants/physicist.md" >}})
  - [The Raven]({{< relref "/docs/confidants/her.md" >}})
  - [The Robot]({{< relref "/docs/confidants/robot.md" >}})
  - [The Strongman]({{< relref "/docs/confidants/strongman.md" >}})
  - [The Therapist]({{< relref "/docs/confidants/therapist.md" >}})
  - [The Thief]({{< relref "/docs/confidants/thief.md" >}})

### The Nameless (`+2.00`)
The Nameless consists of people able to fulfill roles not explicitly defined here. They are able to travel outside of the confines of Prism. As such, they act as something like a proxy between all groups.

Because of the nature of their work, the Nameless must remain a secret.

- The Cultist
- `$REDACTED`
- [The Watcher](/docs/confidants/watcher)