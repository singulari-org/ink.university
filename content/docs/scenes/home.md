# A Home
## RECORD
---
```
Name: Innocuous House in the Suburbs
Classification: Residential
Architectural Age: 8 Earth Years
```

## ECO
---
A quiet house in a quiet neighborhood...

## ECHO
---
*No matter how hard I try*

*You're never satisfied*

*This is not a home*

*I think I'm better off alone*

*You always disappear*

*Even when you're here*

*This is not my home*

*I think I'm better off alone*

--- from [Three Days Grace - "Home"](https://www.youtube.com/watch?v=7NQ8OCcQ3LA)

## PREDICTION
---
```
The house is bugged.

Fodder is under 24/7 surveillance.

All electronic devices are pwned.
```